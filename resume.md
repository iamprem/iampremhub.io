---
layout: resume
title: Resume
permalink: /resume/
---

[//]: # (This is comment - This page is continued from resume.html)

#### **EDUCATION**
---
<br>
**M.S. in Computer Science (Jan 2015 – Present)**  
**GPA	4.0/4.0**  
University of North Carolina at Charlotte, NC, USA
<br>
**Bachelor of Engineering in Electronics and Communication Engineering**  
**GPA	7.15/10**  
Anna University, Chennai, India | June 2013
<br>

#### **PROFESSIONAL EXPERIENCE**
---
<br>
May 2015 - June 2015  
**University of North Carolina at Charlotte**    
**Graduate Teaching Assistant**, Department of Computer Science  
Algorithms and Data Structures  
<br>

Sep 2013 - Nov 2014   
**Cognizant Technology Solutions India Pvt Ltd**  
Programmer Analyst Trainee  

[//]: # (Developed a web application with 3-tier architecture using ASP.NET and SQL Server for Library Management System during training period. (Sep 13 – Jan 14)
[//]: # (Worked in performance testing for web applications : Staples Inc. (Jan 14 – July 14))
[//]: # (Tested www.staples.com and www.staples.ca with various loads and measured performance metrics like response time, peak load for occasions like Black Friday and Cyber Monday.)
[//]: # (Worked in performance testing for SaaS : MedImmune owned by AstraZeneca (July 14 – Nov 14))
[//]: # (Since it was an Offshore driven project I had an opportunity to interact with the clients from Gaithersburg, Maryland directly.)
[//]: # (Performance Tested a Citrix/HTTP protocolled application called “LIMS Labware” using HP Load runner.)
<br>

#### **SKILLS**
---
<br>
**Programming Languages**

- Java
- C++
- Python
- C#

**Area of Interest**  

- Algorithms & Data Structures
- Machine Learning
- Data Mining
- Application Development
 

#### **PROJECTS**
---  
<br>
**Facebook Personal Data analysis using Swift (Current):** An app for the iOS platform that will collect and process usage statistics from a user’s profile in the social networking site – Facebook (using Facebook Graph API), and generate insightful graphs for the user to analyze his/her usage.

**Career Fair Application (Feb – March 2015):** Built a career fair application in android that will be useful to students/alumni and recruiters in a campus recruiting scenario in a University system. This app lists all the companies who will be attending the career fair with their brief introduction, available positions, e-pamphlets, link to recruiter’s website, and directions to apply for positions and the location where the company would put their stall on the career fair day

**Algorithms and Data Structures (Spring 2015):** Implemented the Naïve method, Backtracking Algorithm with both non-linear and linear memory to find the longest common subsequence between two strings using Java. Various implementation of this algorithm is widely used by revision control system.

**Email-ID Extractor (Feb 2015):** Scripted using Jmeter to extract list of encoded email-ids from the participant page on Moodle2 and decoded them to correct email-ids using Java. This is a self-experimented project which helps to create an email Distribution List by students for any course that they register. Code not published in GitHub due to security.


#### **Projects Repository**:  <https://github.com/iamprem>  

---
<br>