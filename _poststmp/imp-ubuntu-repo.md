
### UNetbootin

`sudo add-apt-repository ppa:gezakovacs/ppa`
`sudo apt-get update`
`sudo apt-get install unetbootin`



### Install pipelight so silverlight works

sudo apt-add-repository ppa:pipelight/stable
sudo apt-get update
sudo apt-get install pipelight-multi

## Enable silverlight plugin

sudo pipelight-plugin --enable silverlight

## Enable widevine plugin

sudo pipelight-plugin --enable widevine




### Install restricted extras

sudo apt-get install ubuntu-restricted-extras
