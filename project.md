---
layout: page
title: My Projects
permalink: /projects/
---

### 1. [FaceStats](https://github.com/iamprem/FaceStats)

### 2. [CareerFair](https://github.com/iamprem/careerfair)

### 3. [Library Management System](https://github.com/iamprem/LMS)
